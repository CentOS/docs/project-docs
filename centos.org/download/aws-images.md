---
title: "CentOS AWS AMI Cloud Images"
layout: download-mirror
---

CentOS Amazon AMI images
===========================

Here is the list of current/up2date AMI images IDs that we made public.

So far we build/update/maintain the following CentOS variants and architectures:

 * CentOS Stream 9 (x86_64 and aarch64)

You can search for architecture or region through search bar.

Please note that you can also directly use then the "Deploy Link" function to deploy in specific region, one click away. 


<div class="alert alert-warning" role="alert">
Worth knowing that starting from CentOS Stream 9 the cloud user is `ec2-user` while it was `centos` for CentOS Linux 7
</div>


<table id="download-mirror" class="table table-borderless table-hover">
<thead class="thead-dark">
  <tr>
    <th class="col-0 text-nowrap">Version</th>
    <th class="col-0 text-nowrap">Region</th>
    <th class="col-0 text-nowrap">Architecture</th>
    <th class="col-12 text-nowrap">AMI ID</th>
    <th class="col-0 text-nowrap">Deploy link</th>
  </tr>
</thead>
<tbody>
{% for row in site.data.aws-images %}
  <tr>
    <td class="text-nowrap">{{ row["Version"] }}</td>
    <td class="text-nowrap">{{ row["Region"] }}</td>
    <td class="text-nowrap">{{ row["Architecture"] }}</td>
    <td class="text-nowrap">{{ row["AMI ID"] }}</td>
    <td class="text-nowrap"><a href="{{ row["Deploy link"] }}"><i class="fab fa-aws"></i> Deploy link</a></td>
  </tr>
{% endfor %}
</tbody>
<tfoot>
  <tr>
    <th class="searchable"></th>
    <th class="searchable"></th>
    <th class="searchable"></th>
    <th class="searchable"></th>
    <th></th>
  </tr>
</tfoot>
</table>
