---
title: Download
layout: aside
---

<div class="container alert alert-danger" role="alert" style="display: grid; grid-template-columns: auto 1fr; column-gap: 20px;">
<div style="font-size: 48px;"><i class="fas fa-exclamation-triangle"></i></div>
<div><h4 class="alert-heading">Upcoming EOL Dates</h4>
<p>CentOS Stream 8 end of builds is <b>May 31, 2024</b>. CentOS Linux 7 end of life is <b>June 30, 2024</b>.
Read the <a href="https://blog.centos.org/2023/04/end-dates-are-coming-for-centos-stream-8-and-centos-linux-7/" class="alert-link">information on upgrade and migration options</a>.</p></div>
</div>

{% include download/cards.html %}

As you download and use CentOS Linux or CentOS Stream \([What's the
difference?](/cl-vs-cs/)\), the CentOS Project invites you to [be a part of the
community as a contributor](http://wiki.centos.org/Contribute).
There are many ways to contribute to the project, from documentation, QA, and
testing to coding changes for
[SIGs](http://wiki.centos.org/SpecialInterestGroup), providing mirroring or
hosting, and helping other users.

How to [verify](https://wiki.centos.org/TipsAndTricks/sha256sum) your ISO.

If you plan to create USB boot media, please [read this first](https://wiki.centos.org/HowTos/InstallFromUSBkey) to avoid damage to your system.

If the above is not for you, [alternative downloads](http://wiki.centos.org/Download) might be.

The [CentOS Stream release notes](https://wiki.centos.org/Manuals/ReleaseNotes/CentOSStream) are continuously updated to include issues and incorporate feedback from users.


## Cloud and container images

We build, maintain and update Cloud images that you can find on our [Cloud Images server](https://cloud.centos.org/centos).

These images are built and made available for all the architectures that corresponding version supports.

People interested in importing 'GenericCloud' images into their own cloud solution can find corresponding images on the link above.

Worth knowing that you can also import (through Skopeo or other methods) container images the same way, and such .tar.xz files can be found on the same mirror.

Parallel to that, we have also official images that are available directly to be deployed for the following solutions:

 * [Amazon Web Services](/download/aws-images)
 * [Quay Registry](https://quay.io/repository/centos/centos)
 * [Docker Registry](https://hub.docker.com/_/centos)

If the above is not for you, [alternative downloads](http://wiki.centos.org/Download) might be.

## Geographical mirrors

If you're looking for a specific (or geographically local) mirror, please check out our [list of current mirrors](/download/mirrors/).

## Sources

The CentOS project hosts some sources at [git.centos.org](https://git.centos.org).
CentOS Stream hosts its sources on [Gitlab](https://gitlab.com/redhat/centos-stream).
This is documented in greater detail in the [CentOS wiki](https://wiki.centos.org/Sources).

In order to help ease the workload for our primary mirror network, the source
rpms are not kept in the same tree as the binary packages. If you need the
source packages used to build CentOS, you can find them in our vault 
[vault.centos.org](http://vault.centos.org).

## Older Versions

Legacy versions of CentOS are no longer supported. For historical purposes,
CentOS keeps an archive of older versions. If you're absolutely sure you need
an older version [then click here](http://wiki.centos.org/Download).

## Export Regulations

By downloading CentOS software, you acknowledge that you understand all of the
following: CentOS software and technical information may be subject to the U.S.
Export Administration Regulations (the "EAR") and other U.S. and foreign laws
and may not be exported, re-exported or transferred (a) to any country listed
in Country Group E:1 in Supplement No. 1 to part 740 of the EAR (currently,
Cuba, Iran, North Korea, Sudan & Syria); (b) to any prohibited destination or
to any end user who has been prohibited from participating in U.S. export
transactions by any federal agency of the U.S. government; or (c) for use in
connection with the design, development or production of nuclear, chemical or
biological weapons, or rocket systems, space launch vehicles, or sounding
rockets, or unmanned air vehicle systems. You may not download CentOS software
or technical information if you are located in one of these countries or
otherwise subject to these restrictions. You may not provide CentOS software or
technical information to individuals or entities located in one of these
countries or otherwise subject to these restrictions. You are also responsible
for compliance with foreign law requirements applicable to the import, export
and use of CentOS software and technical information.
