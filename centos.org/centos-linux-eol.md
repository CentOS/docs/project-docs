---
title: CentOS Linux EOL
title_lead: |
    CentOS Linux 8 will reach End of Life on December 31, 2021. Here's
    what that means.
layout: aside
---

CentOS Linux 8 will reach End Of Life (EOL) on December 31st, 2021. Here's what that means.

In line with the EOL of previous releases, we will **NOT** be automatically migrating anyone to the next version (which is CentOS Stream 8 in this case).

We **will** be shipping a rebuild of Red Hat Enterprise Linux (RHEL) 8.5 once it is released, even if that means that this is released slightly after the EOL date. 

The release of a RHEL point release is often accompanied, immediately afterwards, by a set of zero-day updates. We **will** be providing this content as part of the final CentOS Linux 8 release. There will, however, be no more updates to the CentOS Linux 8 content after that time.

Additionally, with this deadline falling during a time when many of our team, as well as many of our users, will be out of the office, we intend to bend the usual EOL process by keeping this content available until January 31st.

At that time, or in the event of a serious security bug in this time window (Defined as anything with a VCSS v3 score of 9 or greater), this content will be removed from our mirrors, and moved to [vault.centos.org](https://vault.centos.org) where it will be archived permanently, since we will not be able to provide updates to the content after the EOL date.

See also:

* [CentOS Project shifts focus to CentOS Stream](https://blog.centos.org/2020/12/future-is-centos-stream/)
* [How CentOS Linux compares to CentOS Stream](/cl-vs-cs)

