CentOS Stream is a continuously delivered distribution that tracks
just ahead of Red Hat Enterprise Linux (RHEL) development,
positioned as a midstream between Fedora and RHEL.
This guide explains the contribution process for CentOS Stream,
as well as the project’s technical background.

To learn more about CentOS Stream, see the [FAQ](https://wiki.centos.org/FAQ/CentOSStream).
Downloads are available on the [project page](https://centos.org/centos-stream/).
Composes are available [here](https://composes.centos.org/).

If you wish to contribute to this guide, click the **Edit this page**
link in the top right corner of any page,
and open an issue or submit a pull request.
