---
title: FAQ - CentOS Project shifts focus to CentOS Stream
layout: aside
---

The future of the CentOS Project is CentOS Stream, and over the next year we’ll be shifting focus from CentOS Linux, the rebuild of Red Hat Enterprise Linux (RHEL), to CentOS Stream, which tracks just ahead of a current RHEL release. CentOS Linux 8, as a rebuild of RHEL 8, will end at the end of 2021. CentOS Stream continues after that date, serving as the upstream (development) branch of Red Hat Enterprise Linux. [Read the rest of our announcement](https://blog.centos.org/2020/12/future-is-centos-stream/).

The following are some of the questions that we’ve been asked about this transition.


### Question 1: What is the future of CentOS?

**Answer:**

While the word “CentOS” (Community ENTerprise Operating System) has long meant a reliable Linux distribution in the Red Hat ecosystem, it also means the people who create and use the technology. With this move to CentOS Stream as the project focus, the CentOS community will continue to bring the same features to our Linux distro. If you are a CentOS Linux user, don’t worry! There will be a CentOS distribution here in the future.

Our goal is to have a distribution which is influenced more by the community, but to retain the strong focus on security, stability, and a clear developer workflow.


### Q2: What about the other releases of CentOS Linux?

**A:**

 * Updates for the CentOS Linux 6 distribution [ended November 30, 2020](https://wiki.centos.org/About/Product).
 * Updates for the CentOS Linux 7 distribution continue as before until the [end of support for RHEL7](https://access.redhat.com/support/policy/updates/errata/#Life_Cycle_Dates).
 * Updates for the CentOS Linux 8 distribution continue until the end of 2021; users can choose to switch over directly to CentOS Stream 8
 * Updates for the CentOS Stream 8 distribution continue through the [RHEL 8 "full support" phase](https://access.redhat.com/support/policy/updates/errata/#Life_Cycle_Dates).

We will not be producing a CentOS Linux 9, as a rebuild of RHEL 9. Instead CentOS Stream 9 fulfills this role. (See Q6 below regarding the overlap between concurrent streams.)


### Q3: Will the source code for Red Hat Enterprise Linux continue to appear on git.centos.org?

**A:** <del>Yes, the source code for Red Hat Enterprise Linux will continue to be published on git.centos.org. Nothing will change about how the source code is published. This change is only related to the binaries the CentOS Project is building and how they are published.</del>

**A, June 2023 Update:** [CentOS Stream](https://gitlab.com/redhat/centos-stream) is now the location for public RHEL source code, sources will no longer be published to git.centos.org. [Announcement](https://www.redhat.com/en/blog/furthering-evolution-centos-stream)


### Q4: How will CVEs be handled in CentOS Stream?

**A:** Security issues will be updated in CentOS Stream after they are solved in the current RHEL release. Obviously, embargoed security releases can not be publicly released until after the embargo is lifted. While there will not be any SLA for timing, Red Hat Engineers will be building and testing other packages against these releases. If they do not roll in the updates, the other software they build could be impacted and therefore need to be redone. There is therefore a vested interest for them to get these updates in so as not to impact their other builds and there should be no issues getting security updates. 


### Q5: Does this mean that CentOS Stream is the RHEL BETA test platform now?

**A:** No.  CentOS Stream will be getting fixes and features ahead of RHEL.  Generally speaking we expect CentOS Stream to have fewer bugs and more runtime features as it moves forward in time but always giving direct indication of what is going into a RHEL release


### Q6: Will there be separate/parallel/simultaneous streams for 8, 9, 10, etc?

**A:** Each major release will have a branch, similar to how CentOS Linux is currently structured; however, CentOS Stream is designed to focus on RHEL development, so only the latest Stream will have the marketing focus of the CentOS Project.

Because RHEL development cycles overlap, there will be times when there are multiple code branches in development at the same time.This allows users time to plan migrations and development work without being surprised by sudden changes. 

Specifically, since the RHEL release cadence is every 3 years, and the full support window is 5 years, this gives an overlap of approximately 2 years between one stream and the next.


### Q7: How do I migrate my CentOS Linux 8 installation to CentOS Stream?

**A:** Instructions to convert from the CentOS Linux 8 distribution to CentOS Stream 8 are published at 
[https://www.centos.org/centos-stream/](https://www.centos.org/centos-stream/)
and are also below for your convenience.

<pre>
[root@centos ~]# dnf swap centos-linux-repos centos-stream-repos

[root@centos ~]# dnf distro-sync
</pre>

This will result in some package updates and new packages being installed, which is expected.

### Q8: I need to build/test my packages for EPEL locally which I used CentOS for. CentOS Stream will have different ABI/API at times so my builds won't work with that.

**A:** EPEL is part of the Fedora Project. We know they are looking into the cases where this happens and are working on a solution. Initial testing showed that only a few packages are affected. We encourage you to get involved there. The CentOS Project cannot and does not speak for Fedora: We encourage you to [engage with the EPEL community instead](https://fedoraproject.org/wiki/EPEL#Communicating_with_the_EPEL_SIG). We encourage you also to read [this proposal](https://lists.fedoraproject.org/archives/list/epel-devel@lists.fedoraproject.org/thread/NYXQNQFAY44RNCJTP6WLYUGJSQX5XSIX/) for EPEL Next with further addresses this question.


### Q9. EPEL 8 needs access to packages which are not shipped by RHEL but were made available by CentOS. How is this going to be solved?

**A:** EPEL is part of the Fedora Project. We know they are looking into the cases where this happens and are working on a solution. Initial testing showed that only a few packages are affected. We encourage you to get involved there.The CentOS Project cannot and does not speak for Fedora: you should instead [engage with the EPEL community](https://fedoraproject.org/wiki/EPEL#Communicating_with_the_EPEL_SIG).


### Q10: When will SIGs start building against CentOS Stream rather than CentOS Linux?

**A:** SIGs can start using CentOS Stream today.   Each SIG should determine when they would like to begin the migration. The opportunity here is for the SIG communities to join the collaboration and contribute into improving processes and focusing resources leading up to the final migration in November 2021. Once a SIG has a plan in place, each SIG can request the relevant tags on our Community Build System.


### Q11: My CI provider uses containers. I currently use CentOS Linux images to test for RHEL compatibility. I cannot use UBI because it does not include the packages I need. What will be my options in the future?

**A:** Today, you should request those packages in UBI to help you with your current release testing. The UBI team has more details. If you’re testing for RHEL, RHEL is the best operating system to use. We also encourage you to begin using CentOS Stream as part of your testing process as well so that you’re always ready for the next RHEL. CentOS Stream will provide a full complement of content, including containers.


### Q12: I used CentOS for CI because I could not use RHEL developer licenses for this. CentOS Stream is aimed at the next generation when I need the last/current one. What is my alternative?

**A:** Red Hat is aware of these concerns and we encourage you to [talk to them](mailto:centos-questions@redhat.com). If you’re testing for RHEL, RHEL is the best operating system to use. We also think that CentOS Stream has a place in your CI testing to help you be ready for the next RHEL. 

 	 	
### Q13: Can I start up a SIG that will maintain CentOS Stream 8 after RHEL8 reaches the end of Full Support?
**A:** We will not be putting hardware, resources, or asking volunteers to work towards that effort, nor will we allow the CentOS brand to be used for such a project. Once RHEL8 reaches the end of full support, CentOS Stream 8 will be retired from build servers, community build systems, primary mirror sites (copies will remain on vault.centos.org), and other places within our ecosystem. Having SIGs build against multiple streams, and packaging/distributing multiple streams, once they are no longer active, is a distraction from what we want to be our main focus - the active stream that precedes the next RHEL release.

### Q14: Can the CentOS community continue to develop/rebuild CentOS linux?
**A:** We will not be putting hardware, resources, or asking for volunteers to work towards that effort, nor will we allow the CentOS brand to be used for such a project, as we feel that it dilutes what we are trying to do with the refocus on CentOS Stream. That said, the code is open source and we wouldn't try to stop anyone from choosing to use it or build their own packages from the code.

### Q15: How does CentOS Stream differ from [Fedora ELN](https://docs.fedoraproject.org/en-US/eln/)?

**A:** CentOS Stream is focused on the next RHEL minor release. This means we are improving and influencing the shipping releases of RHEL. Fedora ELN is a testing area for changes that may occur in the next *major* release of RHEL.The CentOS Project cannot and does not speak for Fedora. We encourage you to [speak with them directly](https://ask.fedoraproject.org/).


### Q16: Where can I ask more questions?

**A:** Since many people will have the same questions, we encourage you to ask your questions on the public project mailing list, [devel](https://lists.centos.org/hyperkitty/list/devel@lists.centos.org/), where everyone can benefit from the questions and answers being publicly archived. However, we will also be watching our various social media accounts for questions and comments.

