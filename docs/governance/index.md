---
title: "Governance"
---

The CentOS Project has two main levels of governance - The Board of Directors (“The Board”), and Special Interest Groups (SIGs).

The Board is responsible for the governance and administration of the project as a whole, and the overall vision and direction of the project.

SIGs are responsible for technical work, and the direction of that work.

In addition to these formal tiers of governance, the project is driven by volunteer developers who sustain the project by their work and contributions.

## The CentOS Project Mission Statement

CentOS aims to make Enterprise Linux better by providing project
resources for contributors with similar interests to collaborate
and/or communicate about the experience for everyone within our
ecosystem. Whether it is artwork, development, discussion, or
troubleshooting - CentOS should provide a clear way to get involved
for contributors of all skill and interest levels.

## The CentOS Board of Directors

The focus of the Governing Board is to assist and guide in the progress and
development of the various SIGs, as well as to lead and promote CentOS.

The CentOS Governing Board is the governing body responsible for the overall
oversight of the CentOS Project and SIGs, the creation of new SIGs, and the
election (and re-election) of new board members. The Board also has the
responsibility to ensure the goals, brands, and marks of the CentOS Project and
community are protected. The Board serves as the final authority within the
CentOS Project.

[Structure and Policies of the Board of Directors](board/)

The current membership of the CentOS Governing Board is:

* Josh Boyer
* Davide Cavalca
* Troy Dawson
* Brian Exelbierd ([Liaison](/about/governance/board-responsibilities/#red-hat-liaison-responsibilities))
* Johnny Hughes
* Amy Marrich (Chair)
* Mike McLean
* Jeffrey Osier-Mixon
* Thomas Oulevey (Secretary)
* Pat Riehecky (Co-Chair)

Also in regular attendance:

* Shaun McCance (Community Architect, attendee ex-officio)

## Special Interest Groups

Special Interest Groups (SIGs) are smaller groups within the CentOS community that focus on a
small set of issues, in order to either create awareness or to focus on development along a specific topic.

[Further details, and list of SIGs](https://wiki.centos.org/SpecialInterestGroup)


## More information

* [Governing Board responsibilities](/about/governance/board-responsibilities)
* [SIGs](/about/governance/sigs)
* [Joining the project](/about/governance/joining-the-project)
* [Voting](/about/governance/voting)
* [Appendix:  Glossary](/about/governance/appendix-glossary)
