---
title: "Board of Directors"
title_lead: Structure and Policies
---

**This document is a work in progress.**

## The Board of Directors

The board consists of 8-11 members, tasked with the oversight of the CentOS Project as a whole. The current membership of the board shall be published on the CentOS Website, and in the Board git repository.

### Director/Board responsibilities

The Board provides guidance and leadership over the project roadmap as a whole. They are responsible for the health of the entire CentOS Project, and ensuring that it operates legally, ethically, and by the principles of open source communities.

Directors are individually, and collectively, responsible for the transparent and ethical behavior of the Board as a whole. Directors are expected at all times to act in the best interests of the project as a whole. Directors must therefore have the ability to debate, understand, and balance sometimes opposing CentOS Project objectives.

The Board is responsible for overseeing the project's image, outreach, and public statements. The Board are the custodians of the project brand and public image.

The Board is responsible for setting and enforcing policies, including things such as governance, contribution, and licensing policies.  Legal policies or aspects are discussed with Red Hat Legal through the Red Hat Liaison as needed.

They are responsible for ensuring that the project serves its users, and remains in a healthy productive relationship with Red Hat, on which the project depends for its continued success. The board may be called on to act as liaison to other external stakeholders, such as software projects which rely on CentOS as their deployment platform.

#### Relationship to SIGs

The Board will evaluate new SIG proposals, and determine whether a SIG is a good fit for the CentOS Project. They will work with the Red Hat Liaison to ensure that SIGs are not created that cause a direct conflict with Red Hat business interests. Once a SIG is approved, The Board will work with the SIG to ensure that it is operating according to the principles of the CentOS Project, until such time as it reaches maturity and is self-governing. The Board’s role in SIGs is discussed in greater detail in the section ‘Special Interest Groups’, below.

Directors are the leaders of the community, both internally, and in the eyes of the public. Directors are expected to represent the consensus of the CentOS Project, and the Board as a whole, when they speak externally, especially to the press, at events, or in other public statements.

Directors will attend Board meetings. It is understood that life may sometimes intervene and make this impossible. However, directors are expected to make every reasonable effort to attend all meetings. Additionally, directors may occasionally be asked to respond on shorter notice to Board business, either via email, or in impromptu meetings. They are expected to make every reasonable effort to do so in a timely manner. If a director expects to be absent at a meeting where a major decision is to be made, they are responsible for indicating their position via email, or by designating a proxy for their vote.

Directors are expected to consider, and respond to, email sent to the board mailing list(s) in a timely manner, so that issues raised between meetings can be addressed without the community having to wait for another meeting. However, it’s important to remember that this is a volunteer position, and it is always understood that other things (family, job, vacation time) take precedence sometimes.

Directors are expected to subscribe to, and periodically check on new issues raised on the Board issue tracker - https://git.centos.org/centos/board/issues - which is the primary way that the community raises issues to the Board.

While SIGs and other parts of the community are expected to be largely self-governing, the Board serves as the final point of appeal for any disagreements or conflicts that arise in other parts of the community.

The CentOS Governing Board is the governing body responsible for the overall oversight of the CentOS Project, CentOS Linux and CentOS Stream, and Special Interest Groups (SIGs). They are responsible for creation of new SIGs, and the election (and re-election) of board members.

#### Brands and Trademarks

The Board also has the responsibility to ensure the goals, brands, and marks of the CentOS Project and community are protected.

#### Relationship to Red Hat

The Board is responsible for managing the relationship with Red Hat, and, more specifically, the Red Hat Enterprise Linux (RHEL) team and Community Platform Engineering (CPE).

#### Directors are Individuals

Board membership is an individual role. Directors do not act on behalf of their employer, with the important exception of the Red Hat Liaison (See below). If a director feels that they cannot act in the best interests of the project, even when it might not be the preferred decision of their employer, or if doing so may negatively impact their employment, they are probably not an ideal candidate. Nominated candidates should discuss this with their employer and management, and make this expectation clear to them, so that this does not come as a surprise to them at some later date. Furthermore, a director’s employer should not issue public statements that imply that their employee is acting as a representative of the corporation.

Board membership is a volunteer role, and is not compensated.

The board may delegate any of these responsibilities to a SIG or board-appointed Officer, whose work they will then oversee.

### Term

The term of board shall be 12 months. There are no limits on the number of terms that an individual director can serve. However, the 12 month cadence allows board members to commit to a short term of service, and reevaluate their availability and interest at regular intervals.

In order to ensure that, at most, half of the board is replaced at any one time, the Board will be divided into two cohorts, which will be staggered by six months, such that half of the board members are either reconfirmed, or replaced, every six months. This is done in order to ensure continuity of knowledge, even if a majority of the board turns over in a given term.

The Board Chair will be responsible for ensuring that this is on the agenda at six month intervals, or as soon thereafter as scheduling is possible, and that a nomination period is announced prior to this meeting, in the event that there are directors stepping down. (See also, “[Mid-term appointments](#mid-term-appointments)” below, for directors that depart before the usual end of term.)

### Appointment of Board members.

The Board shall consist of 8 to 11 members, the exact number to be determined, and announced, prior to the appointment of a new board.

Board members are selected by the existing board members, from a pool of candidates proposed by the larger community. The community should be notified of an upcoming term at least 30 days in advance of the meeting in which the new board will be selected.

#### Nomination

The CentOS Community (specifically, the centos-devel mailing list) will be asked for nominations, 31 days prior to a board meeting in which directors will be appointed. At this time, the board will determine how many seats will be available, based on whether any directors are stepping down, and how many seats, total, the board wishes to have on the next board.

It is important to remember that the Board selects directors, and that the decision is made by the sitting board, not by the larger community.

Suggested names shall be collected, and presented to the board for deliberation and voting.

Directors will typically be nominated based on their merit in the community. This includes, but is not limited to, active participation in the CentOS Project, and a clear understanding of project goals and governance. The community may decide, however, to nominate an “outsider”, if they believe that the new perspective of a potential candidate will be in the best interests of the project.

#### Vote/Appointment process

The sitting board, being presented with the list of nominated directors, will determine, in a process to be decided by the chair and the sitting board, which of the nominated individuals will be selected for the available seats. (Specific mechanics of voting are a matter of implementation, not a matter of bylaws. Current process is documented [here](____))

Directors are expected to carefully weigh the balance between welcoming new members, and maintaining continuity between terms, with the interests of the entire community as their highest priority.

#### Mid-term appointments

In the event that the number of active directors drops below the minimum number, due to a director leaving the board for whatever reason, the board shall replace that seat by holding an out-of-schedule appointment, using the usual process defined above, in order to retain the desired number of seats.

### Out-of cadence removal

A director may be removed by the board by resignation, death, or by an act of the other directors.

#### Resignation

A director may also, at any time, resign for reasons of their own, without being expected to provide a reason either to the board or the larger community, as this is a volunteer position. They are requested to provide a 30 day notice of such resignation if this is at all possible.

#### Activity

All directors are contacted by the Community Architect towards the end of their cohort period during the year to determine if they wish to continue serving. It is important for the community and the project that directors are
active and participating. While it is understood that members of the board (not including the laison) are volunteers, it is important that directors are accessible. As such not attaining the following criteria could result in a director being considered inactive:

* Absence from 3 consecutive Board meetings without prior communication
* Lack of response to the Community Architect participation inquiry within 2 months
* No response to direct communication in multiple forms for 3 months

Note: Communication can be public, or privately to the Community Architect or Chair

#### Removal by the board

A director may be removed from their seat by consensus of sitting directors minus the director in question, for dereliction of duty, misconduct, conflict of interests, violation of the project Code of Conduct, or other offence to be determined by the board, or reported via the Code of Conduct process. The board is reminded that their highest duty is to the CentOS Project as a whole, and are instructed to put this duty above personal allegiances, in the event that such an eventuality arises.

### Named roles

The CentOS Governing Board shall include three named roles: a Chair, a Secretary and a Liaison, which shall be present at all times on the Board. The Board is responsible for appointing one Board member as Chair. Red Hat is responsible for appointing the Liaison role to a Board member, who must be a Red Hat employee. The Board will determine a Secretary from among its members. Any given individual may serve in only one of these positions.

In addition to these named Board roles, Red Hat may provide a full-time Community Architect to the project. The Community Architect has a standing invitation to all board meetings, and to the board mailing list.

These four roles are discussed in greater detail below.

#### Chair

Each time new directors are seated, a Chair shall be selected from the sitting directors.

The Chair shall organize and run the Board meetings, at least once a month, and provide advance notice thereof to directors, and to the larger CentOS Community. The Chair is responsible for maintaining the standing invitation list, and extending that invitation, with attendance details, to those individuals.

The Chair is the final spokesperson for the project. As the “voice of the project”, the Chair is the final arbiter and approver of public statements made by the Project. They may, however, delegate that role to others from time to time, or on particular topics.

The Chair will act as the driver in board meetings, and in on-list discussions, in order to help the Board to reach consensus and resolve conflicts, and keep the discussion on track and on topic. This may include calling a vote to decide contentious issues.

The Chair will guide the Board in transparency, and practicing the open source way, in leadership and decision making.

#### Secretary

At the beginning of each term, a Secretary shall be selected for that term. The Secretary may be a director, but this is not required. A Secretary selected from outside of the directors need only be someone that the directors all trust to keep confidentiality of meeting details when necessary.

The Secretary’s responsibilities shall be as follows.

The Secretary will schedule meetings at times agreed upon by the directors, will announce these meetings to attendees at least a week in advance of the meeting, and will provide invited attendees with the agenda for the meeting. (Meetings are typically held on the Second Wednesday of each month, but this may be changed at the desire of the directors.)

The Secretary will determine when quorum has been reached, and advise the Chair of such, so that meetings may be called into session. They will note what directors are present and absent, and when directors arrive late or leave early, so as to track who was involved in which parts of the discussion.

The Secretary will act as the Chair, in the absence of the Chair, or delegate this role to another attendee selected from the directors or other roles named in this document.

The Secretary will take minutes during the meeting, and, upon approval of the board, publish those minutes to a public location such as the CentOS Blog, for inspection by the community. Minutes shall also be stored in git.centos.org/centos/board for permanent archival.

Between meetings, the Secretary will collect items for the next meeting’s agenda.

#### Red Hat Liaison

Red Hat will appoint one Director to serve as the Red Hat Liaison.

The Liaison will provide the perspective and insights to the Board on Red Hat business (including potential legal, ecosystem, and security concerns) by acting as the interface for various Red Hat constituencies.

Conversely, the Liaison will also allow for the Board to bring concerns or perspectives from the community to Red Hat. They will notify the board when a topic under discussion is likely to cause concern with some group within Red Hat.

The Liaison may be requested by the Board Chair to escalate a decision beyond the Board in some cases when the Board cannot reach consensus.

The Liaison is required to be a Red Hat employee, which will enable this person to be an effective bi-directional conduit between Red Hat and the CentOS Project. The Liaison may be rotated at any time to an alternate Red Hat employee who is a Board member. Newly appointed Board members may also hold the Liaison seat. The Liaison may not serve as Chair.

The Liaison also has the role of dealing with issues such as security, export, or items that have a direct legal or other Red Hat business connection for the Project, and on issues of such business may have an additional role as defined by the following:
A. If a decision/solution cannot be found on an issue that is deemed time and/or business critical by Red Hat then, after a reasonable time, the Liaison may be requested by the Chair to seek assistance from Red Hat in making the decision, obtaining additional input from Red Hat executives, engineers, and stakeholders.
B. The Liaison may, in exceptional circumstances, make a decision on behalf of the Board if a consensus has not been reached on an issue that is deemed time or business critical by Red Hat if: (1) a board quorum (i.e., a majority) is present or a quorum of Board members has cast their votes; or (2) after 3 working days if a Board quorum is not present at a meeting or a quorum has not cast their votes (list votes); provided that the Chair may (or at the request of the Liaison, will) call a meeting and demand that a quorum be present.
The Board of Directors may, at any time, petition the Red Hat RHEL BU to replace or remove the Liaison for dereliction of duty, violation of the Code of Conduct, or other reasons agreed upon by the Board and presented to the Liaison’s management.

#### Community Architect

The Community Architect serves as an ex-officio member of the board of directors - that is, attends board meetings and may speak on issues there, but does not have a vote on Board matters.

The Community Architect’s primary responsibility is to be the voice of the CentOS Community, to the Board of Directors, to the world at large, and to Red Hat. In this last regard, the Community Architect serves as the counterpart to the Red Liaison. Specifically, the Community Architect does not represent Red Hat’s interests to the community, the board, and the world.

The Community Architect is an employee of the Open Source Program Office at Red Hat.

The Community Architect’s day to day responsibilities include, but are not limited to:

* Chairing the Promo SIG, and, specifically, running events on behalf of the CentOS Community
* Primary editor/publisher of the monthly community newsletter
* Representing the interests of the community, and the SIGs, to the Board of Directors, and advocating for the complaints of these constituents
* Managing the budget provided by Red Hat OSPO, for the benefit of the community

The Board of Directors may, at any time, petition the Red Hat OSPO to replace or remove the Community Architect for dereliction of duty, violation of the Code of Conduct, or other reasons agreed upon by the Board and presented to the CA’s management.
