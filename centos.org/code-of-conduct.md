---
title: "CentOS Code of Conduct"
layout: aside
---

## CentOS Community Code of Conduct

The CentOS Code of Conduct is a set of guidelines that explains how our community behaves and what we value to members and outsiders. The Code of Conduct is a living document and will be updated when and if it is deemed necessary.

The Code of Conduct is not “code” in the sense of being an algorithm or a computer program. The Code of Conduct is not “blindly and algorithmically” executed but is instead enforced by humans making real decisions based on all of the available information and using all available context.

The Code of Conduct does not seek to restrict speech or penalize non-native speakers of English. Instead the Code of Conduct spells out the kinds of behaviors we, as a community, find to be acceptable or unacceptable.
Our Pledge
In the interest of fostering an open and welcoming environment, we as the CentOS community pledge to collaborate in a respectful and constructive manner, and welcome everyone willing to join us in that pledge. We welcome individuals regardless of ability, age, background, body size, education, ethnicity, family status, gender identity and expression, geographic location, level of experience, marital status, nationality, national origin, native language, personal appearance, race and/or ethnicity, religion, sexual identity and orientation, socioeconomic status, or any other dimension of diversity.

Our channels, mailing lists, and posts should focus on CentOS and on free and open source software and content. We pledge to make participation in our project and our community a harassment-free experience for everyone. We pledge to avoid personal attacks on others, and to avoid inflammatory language and speech that perpetuates discrimination. Furthermore, we pledge to not use the CentOS Project and its platforms as a basis to engage in personal campaigns against other organizations or individuals.

### Our Standards

Examples of behavior that contributes to creating a positive environment include:

* Using welcoming and inclusive language
* Being kind to others
* Behaving with civility
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior include:

* The use of sexualized language or imagery and unwelcome sexual attention or
  advances
* Initiating controversy for controversy’s sake (including but not limited to [sealioning](https://everydayfeminism.com/2017/01/sealioning/)).
* Saying insulting/derogatory comments and making personal attacks.
* Repeatedly instigating conflict, and baiting people into arguments
* Public or private harassment
* Publishing someone else’s private information, such as a physical or electronic
  address, without explicit permission
* Deliberate intimidation, stalking, or following
* Violent threats or language directed against another person
* Sexist, racist, homophobic, transphobic, ableist, or exclusionary statements, even if they were meant as jokes
* Excessive swearing
* Unwelcome physical contact
* Sustained disruption of talks or other events
* Other conduct which could reasonably be considered inappropriate in a professional setting even if the conduct may be legal under the laws of some jurisdiction.

### Our Responsibilities

The CentOS Community Architect is the responsible party for clarifying the standards of acceptable behavior and are expected to take appropriate and fair corrective action in response to reported instances of behavior that is not aligned to this Code of Conduct, along with the Red Hat Legal team, as appropriate.

The responsibility of enforcing the Code of Conduct falls with these parties for several reasons. It provides more security and protection to the reporter and any parties involved. As this role is full time and paid, they are able to act on the issues in a timely manner. Red Hat is in a position to protect the Community Architect legally if complications arose from attempts at resolution.

The CentOS Community has the right in its sole discretion and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct. Where editing is not practical we shall post a follow up explaining that this does not reflect our standards.

### Scope

This Code of Conduct applies in all online and offline project spaces and in all online and offline spaces where an individual is representing the project, its community, or is acting as a community member.

Examples of acting as a community member include:
* Posting to a CentOS mailing list,
* Filing a bug with CentOS,
* Participating in a CentOS communication channel, such as IRC or telegram
* Contributing to the CentOS Project in any form
* Using an official project email address
* Posting via an official social media account
* Acting as an appointed representative at an online or offline event
* speaking for CentOS or its subprojects at a public event or online video
* working in a CentOS or related booth or table at an event
* Participating in a CentOS or related technical meetup
* Serving as an elected or appointed leader in CentOS, including being a member of the CentOS Board of Directors

### Reporting and Enforcement

Instances of behavior inconsistent with this code may be reported by contacting the CentOS project by filing a private CentOS Code of Conduct ticket at [https://git.centos.org/centos/board](https://git.centos.org/centos/board). This issue will only be visible by [the Board of Directors and the Community Architect](https://www.centos.org/about/governance/). If the incident occurs at an event, the local event staff should be contacted in addition to opening a ticket.

Individuals without access to [https://git.centos.org/centos/board](https://git.centos.org/centos/board) may send an email to the following address: shaunm@redhat.com. This email will be converted into a private Code of Conduct ticket by the CentOS Community Architect. Information about the current CentOS Community Architect, Shaun McCance, can be found at [https://wiki.centos.org/ShaunMcCance](https://wiki.centos.org/ShaunMcCance).

All reports will be kept confidential. When we discuss incidents with anyone we will anonymize details as much as we can. This means that the identities of all involved parties will remain confidential unless those individuals instruct us otherwise or we are required to make disclosures under the law.  Additionally, in some cases we may need to disclose this information to other parties at Red Hat. CentOS is not a separate legal entity and therefore has to comply with all requirements imposed upon Red Hat.

Upon our review/investigation of the reported incident, we will determine what action is appropriate based on this Code and its clarifying statements. An incident review will include communication with the reporter and the individual being reported, and an opportunity for both parties to provide an account of the incident.

All complaints will be reviewed and will result in a response. Failure to follow this Code may result in actions including, but not limited to, warnings, temporary suspension, and in extreme circumstances, banning from the CentOS Project. Please note, while we take all concerns/reported incidents raised seriously, we will use our discretion to determine when and how to follow up on reported incidents.

### Attribution & License

This Code of Conduct is an edited form of the [Contributor Covenant](https://www.contributor-covenant.org/version/1/4/code-of-conduct), version 1.4 along with material from the [PyCon Code of Conduct and others](https://us.pycon.org/2018/about/code-of-conduct/). The process of dealing with reports is inspired by ideas from the [Mozilla CPG Incident Process](https://medium.com/mozilla-open-innovation/how-were-making-code-of-conduct-enforcement-real-and-scaling-it-3e382cf94415).

Additional material is drawn from many sources, including:

* [https://docs.fedoraproject.org/en-US/project/code-of-conduct/](https://docs.fedoraproject.org/en-US/project/code-of-conduct/)
* [http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Responding_to_reports](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Responding_to_reports)
* [http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy_resources](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy_resources)
* [https://www.ashedryden.com/blog/codes-of-conduct-101-faq](https://www.ashedryden.com/blog/codes-of-conduct-101-faq)
* [http://safetyfirstpdx.org/training/code_of_conduct/TemplateIncidentResponseGuide.pdf](http://safetyfirstpdx.org/training/code_of_conduct/TemplateIncidentResponseGuide.pdf)
* [https://www.washingtonpost.com/opinions/how-my-restaurant-successfully-dealt-with-harassment-from-customers/2018/03/29/3d9d00b8-221a-11e8-badd-7c9f29a55815_story.html](https://www.washingtonpost.com/opinions/how-my-restaurant-successfully-dealt-with-harassment-from-customers/2018/03/29/3d9d00b8-221a-11e8-badd-7c9f29a55815_story.html)
* [https://www.shrm.org/hr-today/news/hr-magazine/Pages/1214-workplace-investigations.aspx](https://www.shrm.org/hr-today/news/hr-magazine/Pages/1214-workplace-investigations.aspx)
* [https://www.eeoc.gov/policy/docs/harassment.html](https://www.eeoc.gov/policy/docs/harassment.html)


