---
title: CentOS Linux
title_lead: |
  Consistent, manageable platform that suits a wide variety
  of deployments. For some open source communities, it is a solid, predictable
  base to build upon.
layout: aside
---

<div class="container alert alert-danger" role="alert" style="display: grid; grid-template-columns: auto 1fr; column-gap: 20px;">
<div style="font-size: 48px;"><i class="fas fa-exclamation-triangle"></i></div>
<div><h4 class="alert-heading">Upcoming EOL Dates</h4>
<p>CentOS Stream 8 end of builds is <b>May 31, 2024</b>. CentOS Linux 7 end of life is <b>June 30, 2024</b>.
Read the <a href="https://blog.centos.org/2023/04/end-dates-are-coming-for-centos-stream-8-and-centos-linux-7/" class="alert-link">information on upgrade and migration options</a>.</p></div>
</div>

## Download

{% include download/cards.html distribution="centos-linux" %}
