---
title: Comparing CentOS Stream and CentOS Linux
title_lead: |
  What's the difference between CentOS Stream and CentOS Linux?
layout: aside
---

# Comparing Centos Linux and CentOS Stream

The CentOS Project produces two variants: CentOS Linux and CentOS Stream. They are alike in many ways. Here's what sets them apart.

See also: [CentOS Stream FAQ](https://www.redhat.com/en/blog/faq-centos-stream-updates)

## End of Life

As [announced in December of 2020](https://blog.centos.org/2020/12/future-is-centos-stream/), The CentOS Project has shifted focus from CentOS Linux to CentOS Stream. Here are the expected end of life (EOL) dates for our various releases.

* CentOS Linux 7 EOL: 2024-06-30
* CentOS Linux 8 EOL: 2021-12-31
* CentOS Stream 8 EOL: 2024-05-31
* CentOS Stream 9 EOL: 2027-05-31

## Upstream vs downstream

CentOS Linux is a rebuild of Red Hat Enterprise Linux (RHEL). As such, it is downsteam from RHEL. CentOS Linux release version numbers reflect the date of the RHEL release on which they are based. For example, CentOS 8.2105 is a rebuild of RHEL 8.3, which released in May of 2021.

CentOS Stream, on the other hand, is the upstream, public development branch for RHEL. Specifically, CentOS Stream 8 is the upstream for the next minor release of RHEL 8, CentOS Stream 9 for the next minor release of RHEL 9, and so on.

## Frequency of updates

CentOS Linux is a rebuild of the current released RHEL content.  New minor versions consist of large batches of updates.  Smaller batches of updates are delivered between minor versions.

CentOS Stream contains content planned for upcoming RHEL minor releases.  The updates are not batched up into minor releases, but rather are released as they are ready.

## Contribution model

Getting changes into CentOS Linux involves contributing to the upstream projects that are included in the distribution. This includes, but is not necessarily limited to, contributing to Fedora. Change that make it into Fedora might be in the next release of RHEL, which will then be in the next CentOS rebuild. This process can take years.

CentOS Stream provides a way to [contribute directly](https://docs.centos.org/en-US/stream-contrib/), cutting this process down to weeks or days.
 
## Testing

Testing for CentOS Linux and CentOS Stream is very similar, and differs mostly in the timing in which things happen.

Changes hit CentOS Stream and RHEL only once they have undergone [rigorous testing](https://blog.centos.org/2020/12/centos-stream-is-continuous-delivery/). These tests occur both internally to Red Hat, and in our public distro-wide test suite. As such, you can [contribute your tests](https://github.com/CentOS/sig-core-t_functional) to that system, so that future changes don't break the things you care about.

As a rebuild, CentOS Linux benefits from all of these same tests, but, prior to CentOS Stream, lacked a way for you to influence what was tested and how.

