# CentOS.org website

The CentOS.org website is using the following tools:

 * [jekyll](https://jekyllrb.com/) 4
 * [bootstrap](https://getbootstrap.com/) 4.
 * [podman](https://podman.io) (but should work with other know containers solutions too)

## Contributing to changes to this repo

### Needed tools

Just ensure that you have git, podman installed on your CentOS, Fedora workstation (or any other linux distro, just showing this as example):

    sudo yum install slirp4netns podman git

### Cloning this repo, from your forked version

You should first login with your [ACO](https://accounts.centos.org) login on
https://git.centos.org, and then fork this repo (if not already done)

Once done, you'll have to clone your fork locally, and submit changes.  This
section describes the steps you need to follow in order to render the final
site using jekyll in Fedora 31/CentOS 8, with rootless container.

Let's assume the following (so feel free to update):

    export git_upstream="ssh://git@git.centos.org/forks/<ACO_LOGIN>/centos/centos.org.git" # replace with your ACO username
    export git_directory="${HOME}/git/" # where you'll git clone git repo

Let's first clone git repo and ensure that some files in container will be owned by jekyll:

    test -d ${git_directory} || mkdir -p ${git_directory}
    pushd ${git_directory}
    test -d centos.org || git clone ${git_upstream}

    for i in .jekyll-cache vendor vendor/bundle _site ; do
      podman unshare mkdir -p ${git_directory}/centos.org/${i}
      podman unshare chown -R $(id -u):$(id -u) ${git_directory}/centos.org/${i}
    done
    podman unshare chown -R $(id -u):$(id -u) ${git_directory}/centos.org/Gemfile.lock
    popd

Let's now for the first time launch jekyll:

    podman images |grep -q jekyll || podman run --volume="${git_directory}/centos.org:/srv/jekyll:z" --volume="${git_directory}/centos.org/vendor/bundle:/usr/local/bundle:z" --rm -it jekyll/jekyll bundle update

If that works, you'll have everything you need. You can then render/build the
website (under _site directory) like this:

    podman run --volume="${git_directory}/centos.org:/srv/jekyll:z" \
      --volume="${git_directory}/centos.org/vendor/bundle:/usr/local/bundle:z" \
      --rm -it jekyll/jekyll jekyll build

## Running

### Development

The better way to work on local changes is to have jekyll to automatically
"watch" for local changes, and rebuild automatically on the fly when it detects
that files were added/modified. To do this, and then to be able to browse
"live" onhttp://localhost:4000, launch Jekyll like this:

    podman run --volume="${git_directory}/centos.org:/srv/jekyll:z" \
      --volume="${git_directory}/centos.org/vendor/bundle:/usr/local/bundle:z" \
      -p 4000:4000/tcp \
      --rm -it jekyll/jekyll jekyll serve

### Production

The `jekyll serve` command fits well on development environments, however when
you want to use a more robust web server (e.g., Apache, Nginx) in production
environments the `jekyll serve` command may be not appropriate.  Instead, you
may use the `jekyll build --watch` command inside a container and control it
using systemd.  In this configuration the web server should be configured to
expose the `${git_directory}/centos.org/_site/` directory as document root, the
place where jekyll build command renders the final files.

1. Create a detached container running `jekyll build --watch` command:

        podman run -d --volume="${git_directory}/centos.org:/srv/jekyll:z" \
            --volume="${git_directory}/centos.org/vendor/bundle:/usr/local/bundle:z" \
            -it --name www.centos.org docker.io/jekyll/jekyll:latest jekyll build --watch

2. Use podman-generate-systemd(1) command to create the systemd service unit
   under the `~/.config/systemd/user/` directory. If this directory doesn't
   exist, create it.

        cd ~/.config/systemd/user/
        podman generate systemd --files --name www.centos.org

3. Stop the container previously created. Don't remove it. It will be
   controlled by systemd.

        podman stop www.centos.org

4. Enable and start the container using systemctl command.

        systemctl --user enable container-www.centos.org.service
        systemctl --user start container-www.centos.org.service

5. To start the service at system start and persist over user logouts, enter
   (as root user):

        loginctl enable-linger <username>

Reference:

* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/building_running_and_managing_containers/porting-containers-to-systemd-using-podman_building-running-and-managing-containers

### Opening a PR (Merge request)

Once you're satisfied with local changes, proceed as usual:

 * `git commit` and `git push` to origin (your fork)
 * open PR on git.centos.org

### Reviewing a PR (for admins)

When someone will open a PR, there is a way to pull locally the proposed
changed and render locally.  We can apply the method above with "jekyll serve"
but we can pull locally.  On each PR, there is a link at bottom named "Pull
this pull-request locally" with a link to instructions.  If you proceed, that
will create a new temporary branch named pr<number>, so you can then `git
checkout pr<number>`, render website automatically and see if that looks ok.
If it is, you can go back to git.centos.org, and then either comment (if you
need some changes) or just `merge` it.  Merging it in main branch will
automatically means that website will be rebuilt and pushed in the next
minute[s] to www.centos.org nodes.
