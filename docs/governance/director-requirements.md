---
title: "Director Requirements"
---

## Director Requirements

The CentOS Board of Directors provides oversight and direction for the CentOS Project. As such, certain things are expected of Board members. The following are the high-level expectations for Directors, which should guide who should be nominated for Director positions, and should guide whether one should accept such a nomination in good faith.

You are expected to have read and understood documentation related to governance of the project. At this time, that consists of:

 * [centos.org/about](https://www.centos.org/about/)
 * [centos.org/about/governance/](https://www.centos.org/about/governance/)
 * [centos.org/about/governance/board-responsibilities](https://www.centos.org/about/governance/board-responsibilities/)
 * [wiki.centos.org/SIGGuide](https://wiki.centos.org/SIGGuide)
 * [centos.org/about/governance/joining-the-project](https://www.centos.org/about/governance/joining-the-project/)
 * [centos.org/about/governance/voting](https://www.centos.org/about/governance/voting/)

*We are working on updated, consolidated governance documents at this
time, so the above list is subject to change.*

Directors are expected to attend the monthly meetings, which, at present, are scheduled on the second Wednesday of each month. Each new Board cohort is at liberty to change the exact timing of Board meetings to best accommodate the currently-sitting members. Board meetings typically run one hour.

Directors are expected to consider, and respond to, email sent to the Board mailing list(s) in a timely manner, so that issues raised between meetings can be addressed without the community having to wait for another meeting. However, it’s important to remember that this is a volunteer position, and it is always understood that other things (family, job, vacation time) takes precedence sometimes.

Directors are expected to subscribe to, and periodically check on new issues raised on the [Board issue tracker](https://git.centos.org/centos/board/issues), which is the primary way that the community raises issues to the Board.

Directors *do not* act on behalf of their employer, with the important exception of the [Red Hat Liaison](https://www.centos.org/about/governance/board-responsibilities/#red-hat-liaison-responsibilities). Board membership is an individual role. If you feel that you cannot act in the best interests of the project, even when it might not be the preferred decision of your employer, or if doing so may negatively impact your employment, this is not the role for you. You should discuss this with your employer and your management, and make this expectation clear to them, so that this does not come as a surprise to them at some later date. Furthermore, your employer should not issue public statements that imply that you are their representative.

Directors will typically be nominated based on their merit in the community. This includes, but is not limited to, active participation in the CentOS Project, and a clear understanding of project goals and governance. The community may decide, however, to nominate an “outsider”, if they believe that the new perspective of a potential candidate will be in the best interests of the project.
