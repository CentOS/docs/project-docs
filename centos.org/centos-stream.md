---
title: CentOS Stream
title_lead: |
  Continuously delivered distro that tracks just ahead of Red Hat Enterprise Linux
  (RHEL) development, positioned as a midstream between Fedora Linux and RHEL.
  For anyone interested in participating and collaborating in the RHEL
  ecosystem, CentOS Stream is your reliable platform for innovation.
layout: aside
---

<div class="alert alert-warning" role="alert"><a href="https://blog.centos.org/2023/04/end-dates-are-coming-for-centos-stream-8-and-centos-linux-7/">End dates are coming in 2024 for CentOS Stream 8 and CentOS Linux 7. Check the blog post for information on upgrade and migration options.</a></div>

## Download

{% include download/cards.html distribution="centos-stream" %}

## FAQ

We've got some answers to the most [frequently asked
questions](https://www.redhat.com/en/blog/faq-centos-stream-updates)
about CentOS Stream.
