---
title: About CentOS
layout: aside
---

## The CentOS Project

The CentOS Project is a community-driven free software effort focused around
the goal of providing a rich base platform for open source communities to build
upon. We will provide a development framework for cloud providers, the hosting
community, and scientific data processing, as a few examples. We work with
several 'upstream' communities to help them layer and distribute their software
more effectively on a platform they can rely on.


### The Governing Board

The CentOS Governing Board is made up of members of the CentOS Project, from
the community at large as well as Red Hat. The focus of the Governing Board is
to curate the CentOS Project, assist and guide in the progress and development
of the various SIGs, including CentOS Stream. For more information read the
[governance page](/about/governance).

### The Project Structure

The CentOS Project is modelled on the structure of the [Apache
Foundation](http://apache.org), with a governing board that oversees various
semi-autonomous [Special Interest Groups or
'SIGs'](http://wiki.centos.org/SpecialInterestGroup). These groups are focused
on providing various enhancements, addons, or replacements for functionality
provided by the CentOS Stream SIG. A few notable examples of SIGs are:

* [Alternative Images](https://sigs.centos.org/altimages/) - Providing a home for Alternative Build images of CentOS Stream
* [Automotive](https://sigs.centos.org/automotive/) - Providing a home for CentOS-oriented automotive work
* [CentOS Stream](http://sigs.centos.org/stream) - Building and releasing the core CentOS Stream platform.
* [Cloud](http://sigs.centos.org/cloud) - Providing a home for Cloud related infrastructure, including RDO(OpenStack) and SCOS/OKD(Kubernetes).
* [Hyperscale](http://sigs.centos.org/hyperscale) - Providing a home for Hyperscale related work
