help:
	@echo "Run 'make test' to test locally or 'make build' to render the static site"

build:
	./mkdocs.sh build

clean:
	rm -rf site

install-commands:
	test -d /usr/share/doc/mkdocs-material || sudo dnf install mkdocs-material

test:
	./mkdocs.sh serve

.PHONY: help build test
