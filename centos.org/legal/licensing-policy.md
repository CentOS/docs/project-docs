---
title: "CentOS Project Licensing Policy"
layout: aside
---

# CentOS Project licensing policy

## Definitions

A “Contribution” is any contribution to the CentOS Project, but does not include (a) already-existing upstream software or content that merely is packaged, or is intended to be packaged, in CentOS Linux or CentOS Stream, or changes or additions to such already-existing software or content, or (b) material intended to be used for CentOS project logos. 

Your Contribution is “Explicitly Licensed” if you specifically attach a license notice to it. 

“Default License” means the license you grant under this policy for a Contribution that is not Explicitly Licensed.

“Code” means software code, RPM spec files, or any functional material whose principal purpose is to control or facilitate the building of packages.

## Distributions

The CentOS Linux and CentOS Stream distributions are [compilations](https://www.law.cornell.edu/definitions/uscode.php?width=840&height=800&iframe=true&def_id=17-USC-2076770877-364936160&term_occur=999&term_src=title:17:chapter:1:section:101) of software packages. Each package is governed by its own license. Like Red Hat Enterprise Linux, the CentOS Linux and CentOS Stream compilation copyright is licensed under [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html). To the extent you hold any copyright in the selection, coordination, or arrangement of packages making up the CentOS Linux or CentOS Stream distributions, you license that copyright under GPLv2. 

The compilation license does not supersede the licenses of code and content contained in the distributions, including anything you may have contributed to that pre-existing material. The complete licensing terms applicable to a given package can be found in the source code of the package. 

## Explicitly Licensed Contributions

Explicitly Licensed Contributions can be under any of the open source or open content licenses considered acceptable for the [Fedora Project](https://fedoraproject.org/wiki/Licensing:Main?rd=Licensing).

## CentOS.org Content

As of 2020-04-23, new content published on [www.centos.org](https://www.centos.org/), [wiki.centos.org](https://wiki.centos.org), [docs.centos.org](https://docs.centos.org), and [blog.centos.org](https://blog.centos.org) is licensed under the Creative Commons Attribution-ShareAlike 4.0 [International Public License](https://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0), except as otherwise indicated in this document or the content itself. Accordingly, the Default License for Contributions to such content is CC BY-SA 4.0, with attribution solely to the CentOS Project. 

## Licensed Repositories

The Default License for Contributions to a CentOS Project repository that contains notice of a license is that same license. 

## Code Contributions

The Default License for Contributions of Code is the MIT license, as follows:

    Copyright Contributors to the CentOS Project.

    Permission is hereby granted, free of charge, to any person obtaining a copy of 
    this software and associated documentation files (the "Software"), to deal in the
    Software without restriction, including without limitation the rights to use, 
    copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
    Software, and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all 
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
    PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Other Contributions

The Default License for Contributions not otherwise addressed in this policy, including mailing list postings, forum posts, website design elements, and content in issues and comments submitted to the [CentOS Bug Tracker](https://bugs.centos.org), is the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/) (CC0).

## CentOS Logos

CentOS logos are covered by the [CentOS Trademark Guidelines](https://www.centos.org/legal/trademarks/) and not within the scope of this policy. If you want to contribute to the development of CentOS logos, please contact the CentOS Governing Board.

## Date

This policy is in effect as of 2020-04-23. 

