---
title: CentOS Stream 9
layout: page 
permalink: /:path/:basename/index.html
toc: true
---

**CentOS Stream 9** is the next major release of the CentOS Stream distribution. CentOS Stream is developed in collaboration with the Red Hat Enterprise Linux (RHEL) engineering team, and with you, the CentOS community.

[Get it now](https://www.centos.org/centos-stream/#tab-3) 💿

[Read the announce blog post](https://blog.centos.org/2021/11/introducing-centos-stream-9/) 📰

[What is CentOS Stream?](https://youtu.be/4_tV0UJAqXM) 🎥

[How contributions to CentOS Stream work](https://youtu.be/0dkQ-WjHnew) 🎥

[![](/assets/img/path_to_rhel_thumb.png)](/assets/img/path_to_rhel.png)

### Timeline

* Initial [announcement](https://www.redhat.com/en/blog/faq-centos-stream-updates): 2020-12-08
* [Sources](https://gitlab.com/redhat/centos-stream) available: 2021-02-19
* [Build system](https://kojihub.stream.centos.org/) available: 2021-04-26 ([announcement](https://lists.centos.org/pipermail/centos-devel/2021-April/076772.html))
* [Compose infrastructure](https://composes.stream.centos.org/) available: 2021-04-29 ([announcement](https://lists.centos.org/pipermail/centos-devel/2021-April/076802.html))
* First community contribution: 2021-04-20 ([submitted](https://gitlab.com/redhat/centos-stream/rpms/pipewire/-/merge_requests/1)), 2021-05-05 ([merged](https://gitlab.com/redhat/centos-stream/rpms/pipewire/-/merge_requests/1#note_4322c388ba6206e482b664e25cbe13196abfddcc))
* Manually-signed composes available: 2021-08-12
* Available in [SIG build system](https://cbs.centos.org): 2021-09-03 ([announcement](https://lists.centos.org/pipermail/centos-devel/2021-September/077320.html))
* [Containers](https://quay.io/centos/centos?tab=tags) available: 2021-09-18
* Automated signing for composes: 2021-09-15
* Available on [mirrors](http://mirror.stream.centos.org/): 2021-09-30 ([announcement](https://lists.centos.org/pipermail/centos-mirror/2021-September/024979.html))
* Available in CI infrastructure: 2021-10-20 ([announcement](https://lists.centos.org/pipermail/centos-devel/2021-October/077383.html))
* Expected EOL: End of RHEL9 "[full support](https://access.redhat.com/support/policy/updates/errata#Full_Support_Phase)" phase (Estimated 2027)

### Contribute

Contributions to CentOS Stream are being accepted now. Proposed contributions are evaluated by the RHEL engineering team, since a contribution to CentOS Stream is a contribution to RHEL.

See the [CentOS Stream Contributor's Guide](https://docs.centos.org/en-US/stream-contrib/quickstart/) for details on how this process works, and to make your first contribution.

Notable community contributions so far:

* Neal Gompa updated PipeWire to 0.3.32 and enabled JACK ([BZ#1956854](https://bugzilla.redhat.com/show_bug.cgi?id=1956854))
* Davide Cavalca added default configs to enable systemd-oomd and make it usable out of the box ([BZ#1962255](https://bugzilla.redhat.com/show_bug.cgi?id=1962255))
* Neal Gompa added Wayland support for the GNOME Classic session ([BZ#2015914](https://bugzilla.redhat.com/show_bug.cgi?id=2015914))
* Andrew Lukoshko added product config for AlmaLinux ([BZ#2004653](https://bugzilla.redhat.com/show_bug.cgi?id=2004653))

