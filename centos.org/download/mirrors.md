---
title: Mirror List
title_lead: List of CentOS official mirrors.
layout: download-mirror
---

<div class="alert alert-warning" role="alert">
Starting from CentOS Stream 9 , mirrors are listed in Mirrormanager so use <a href="https://mirrormanager.fedoraproject.org/mirrors/CentOS">this link</a> 
</div>

CentOS welcomes new mirror sites.  If you are considering setting up a public
mirror site for CentOS, [please follow the mirror
guidelines](http://wiki.centos.org/HowTos/CreatePublicMirrors) to make sure
that your mirror is consistent with the other mirror sites.  


