---
title: "Sponsors"
layout: sponsors
---

If you are interested in becoming a CentOS sponsor, you can contact us at <donate@centos.org>.
