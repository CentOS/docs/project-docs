The CentOS Project uses and makes available several services
that compose its build system.
These tools are used by both the core project to produce CentOS Stream,
and by the various {centos-sigs}[Special Interest Group (SIG)] projects.


# Source Code

All artifacts (RPMs, container images, ISO images) build using source
code stored in a Git version control system.
Repositories have different branches reflecting the build environment
those sources will be used in.
The CentOS Project has two primary locations for this content.

## GitLab

[GitLab.com](https://gitlab.com/redhat/centos-stream) is the platform
for developing CentOS Stream 9 and later.
Projects are publicly accessible and a GitLab account can be used to contribute.

All GitLab activity is forwarded to
[Fedora Messaging](https://apps.fedoraproject.org/datagrepper).
The GitLab specific messages ca be seen at the following link:
[CentOS GitLab Messages](https://apps.fedoraproject.org/datagrepper/raw?rows_per_page=1&delta=127800&category=gitlab).

## CentOS Git

[CentOS Git](https://git.centos.org) hosts content derived from
Red Hat Enterprise Linux source RPMs and content used by SIGs.
The platform is a project-hosted instance of [Pagure](https://pagure.io/pagure),
a Git forge developed and used by [Fedora](https://getfedora.org).
Projects are publicly accessible, and a
[Fedora Account System (FAS)](https://accounts.fedoraproject.org)
identity required for contributor access.

## DistGit

Though Git is a critical part of the Project's build system,
it is not the best solution for dealing with non-text objects
such as upstream code tarballs and binary files.
[DistGit](https://github.com/release-engineering/dist-git) is an
integration with Git that provides tooling to be able to accommodate
the tracking of these assets alongside the RPM repositories.
It creates a lookaside cache that stores the objects and modifies a
designated file in the RPM repository that references them with a SHA hash.
On CentOS Git this is the `.<package>.metadata` file,
and on GitLab the `sources` file.
During the build process objects referenced in the DistGit file will
be pulled from the lookaside cache into the appropriate directories.

For example, the Git package repository:

CentOS Git: [.git.metadata](https://git.centos.org/rpms/git/blob/c8s/f/.git.metadata)
```
[source,text]
996c0be58e901deb4ef9d0145e7bf98cdf6a0fb3 SOURCES/git-2.27.0.tar.xz
097b8da13939ac9f51f97a5659184c1d96fb0973 SOURCES/gpgkey-junio.asc
```

GitLab: [sources](https://gitlab.com/redhat/centos-stream/rpms/git/-/blob/c9s/sources)
```
[source,text]
SHA512 (git-2.31.1.tar.xz) = 9aa334a3e8519700ff5d112153ec42677722980094caa9d22aa91afdb65166bd9a98fa445c0d327c428ebfa73bf4832e9b3836109a1d9319feafe3191cfd170e
SHA512 (git-2.31.1.tar.sign) = 0a721876f9869d1dc9a43e7f83f8e63a3d8fa932ff2d2e69bb98f3e314e2e9a896c2171cb6a020d6c6e929fdf1af736dbeb3f25f93fb4d359a9aaa5b859069c3
```

# Build Platform

## Koji

The service that builds the various distribution deliverables
(RPMs, container archives, disk images, LiveMedia installers, etc)
is [Koji](https://pagure.io/koji).
Within the CentOS Project infrastructure, there are two Koji instances:

| Service Instance | Purpose |
| ---------------- | ------- |
| [CentOS Stream Build Service](https://kojihub.stream.centos.org) (KojiHub) | Builds CentOS Stream, pulling sources from Gitlab. |
| [Community Build Service](https://cbs.centos.org) (CBS) | Used by community projects, such as SIGs, to produce deliverables. Sources are pulled from CentOS Git. Currently does not support Modularity packaging. |

A primer on using Koji can be found in the Fedora documentation
(the `fedpkg` parts can be ignored):
[Using the Koji build system](https://docs.fedoraproject.org/en-US/package-maintainers/Using_the_Koji_Build_System).
Please see the
[Koji documentation](https://docs.pagure.org/koji)
for more detailed instructions and information about Koji's capabilities.

## Module Build Service

[MBS](https://pagure.io/fm-orchestrator) integrates with Koji and handles the building of
[Modularity](https://docs.fedoraproject.org/en-US/modularity) related projects.
