#!/bin/sh
if test -x /usr/bin/mkdocs && test -d /usr/share/doc/mkdocs-material; then
  CMD=/usr/bin/mkdocs
else
  CMD="podman run --rm -it -p 8000:8000 -v ${PWD}:/docs:z docker.io/squidfunk/mkdocs-material"
fi

# shellcheck disable=SC2086,2068
# we want that podman command to be split into multiple strings
# and its arguments too
${CMD} $@
