# Contributor Onboarding

CentOS Stream is where Red Hat Enterprise Linux (RHEL) development happens in public.
That means contributing to CentOS Stream is effectively contributing to an enterprise product —
with many promises around stability, API and ABI compatibility (see links below) —
only certain types of contributions make sense.

Please see the following resources:

[Red Hat Enterprise Linux 9: Application Compatibility Guide](https://access.redhat.com/articles/rhel9-abi-compatibility)

[Red Hat Enterprise Linux Life Cycle](https://access.redhat.com/support/policy/updates/errata/)

[Red Hat Enterprise Linux Container Compatibility Matrix](https://access.redhat.com/support/policy/rhel-container-compatibility)

All of this needs to be considered when making a contribution to CentOS Stream RPMs.

## Accounts

To start making contributions you’ll need to log in to the following places:

* Gitlab - [Sign Up](https://gitlab.com/users/sign_up)

* CentOS Account System - [Sign Up](https://accounts.centos.org/)

* Red Hat Issues - [Sign Up](https://issues.redhat.com/)


## Tools and workstation setup

To work with dist-git repositories and inspect artifacts or logs in the buildsystem,
you need `centpkg` installed on your workstation:

* If you have a Fedora workstation: `dnf install centpkg`
* If you have a CentOS Linux/Stream or RHEL workstation, enable the
  [EPEL repository](https://fedoraproject.org/wiki/EPEL#Quickstart)
  and then: `dnf install centpkg`

# What you do, what the RHEL maintainer takes care of

## 1.) File an issue!

CentOS Stream defects and feature requests are filed under the RHEL project in
[issues.redhat.com](https://issues.redhat.com/).

* [Create an issue against CentOS Stream / RHEL 9](https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?pid=12332745&issuetype=1&versions=12412761)
* [Create an issue against CentOS Stream / RHEL 10](https://issues.redhat.com/secure/CreateIssueDetails!init.jspa?pid=12332745&issuetype=1&versions=12412762)

The `Component` is the name of the package that you want to patch

Describe the change that you’d like to make.
Be sure to double-check for existing issues that describe the feature request or problem that you’re seeing.

RHEL developers use issues to coordinate with RHEL QE, schedule work
with other maintainers, and track progress against development milestones.
RHEL maintainers need to get an issue `approved` for a release before they can
merge your change, but don’t worry, this is something they take care of for you.
You just need to file the issue and reference it.

## 2.) Develop your patch

There are 2 ways to contribute.
*Source Git* is in use by developers of packages that want to accept contributions
directly to the source code and the package specs in the same place.

*Dist-Git* is a familiar packaging format for folks who have done packaging in Fedora.
The package specs are stored in git, and archives of the actual source code are located
in the *lookaside*.

In the Gitlab repository you want to change, click the `Fork` button on the front page
to create your own fork.

## 3.) Reference the issue you filed in your commits

Examples:

* `git commit --signoff -m 'RHEL-1234 This is my awesome patch`

Every commit made during your contribution MUST come with an issue referenced in the commit message.

Once the commits are how you like them, push to your fork in Gitlab.

## 4.) Open a Merge Request

The easiest way to open a merge request is to visit the target repository under the
[gitlab.com/redhat/centos-stream](https://gitlab.com/redhat/centos-stream) namespace
and click the `New Merge Request` button on the `Merge Requests` tab.

TODO: Image of the merge-requests tab

Make sure to choose your fork and branch under the `Source Branch`,
and the CentOS Stream repository and branch in `Target Branch`

TODO: Image of Source branch / Target Branch

## 5.) Work with the RHEL package maintainer

RHEL maintainers will work with you to evaluate your patch, review any needed changes,
and potentially merge your request.
If your patch is accepted for inclusion in CentOS Stream and RHEL, the RHEL maintainer
will perform a package build and RPMs will show up in the buildsystem:
[kojihub.stream.centos.org](https://kojihub.stream.centos.org/).

# Good Patch / Bad Patch

TODO Couple of examples

TODO Link to longer policies

# Release Engineering, pipelines, and package state

## What’s going on with package X?

Packages follow a regular process from build to release,
all managed by tags in the buildsystem.
Here’s a list of important koji tags and what they mean:

| Koji Tag | Purpose |
| -------- | ------- |
| `c9s-gate` | This is where packages first land immediately after they build |
| `c9s-candidate` | These packages have passed gating based on RHEL CI testing and [CentOS Stream CI Testing](https://centos.softwarefactory-project.io/zuul/t/centos/status) |
| `c9s-pending` | These packages have passed gating and preverification, these packages are also added to the buildroot when tagged here |
| `c9s-pending-signed` | These packages have been signed by the signing automation, and are ready for compose |
| `c9s-build` | This is a tag that includes all of the right inheritance to generate the buildroots |
| `c9s-compose` | New composes will be generated from packages in this tag |
| `c9s-released` | Packages which have been released to mirrors are tagged into this once they are visible on the Stream mirrors. |

(Note the `c9s` part can be also `c10s` for CentOS Stream 10.)

For example, if you see a package that is listed in the `-gate` tag
but not the `-pending` tag you know that it hasn’t passed its tests yet,
and won’t make it into a compose until those are fixed.

## Pungi and the Compose Configs

If you want to make changes to the overall distribution itself,
or the artifacts produced by composes, you’ll want to refer to the
`Distribution` component in the RHEL project.
Please file an appropriate issue describing the change you’d like to make.

If you’re familiar with Pungi, you’ll recognize the configs in
[gitlab.com/redhat/centos-stream/release-engineering/pungi-centos](https://gitlab.com/redhat/centos-stream/release-engineering/pungi-centos).

These configs follow a Fork/Merge-Request workflow and require review
from CentOS Stream release engineers to be merged.

## The Comps

Comps control package groups, and repository split configurations.
Changes here most likely require a `Distribution` component issue in the RHEL project.

[gitlab.com/redhat/centos-stream/release-engineering/comps](https://gitlab.com/redhat/centos-stream/release-engineering/comps)

## Composes and release

Packages are built into a compose that happens multiple times a week,
triggered by the [Releng Jenkins](https://jenkins.stream.centos.org/view/c9s%20release/).
The results can be found on [composes.stream.centos.org](https://composes.stream.centos.org/)

How often they happen is defined in a Jenkins config file - see the
[c9s config for example](https://gitlab.com/redhat/centos-stream/release-engineering/releng-tools/-/blob/master/jenkins/jobs/stream9_composes.jenkins?ref_type=heads#L35).

Once a compose is complete, it gets tested by the [testing Jenkins instance](https://testing.stream.centos.org/). You can contribute to [the compose tests](https://gitlab.com/CentOS/Integration/compose-tests) through the  [CentOS Integration SIG](https://sigs.centos.org/integration/).

The results of these tests are reviewed manually and then pushed out to mirrors at [mirror.stream.centos.org](https://mirror.stream.centos.org/) by Releng. This is currently a manual process and happens usually once a week.
